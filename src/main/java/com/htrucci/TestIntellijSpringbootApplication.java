package com.htrucci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestIntellijSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestIntellijSpringbootApplication.class, args);
	}
}
