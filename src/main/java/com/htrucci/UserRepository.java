package com.htrucci;

import com.htrucci.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Htrucci on 2017. 11. 6..
 */
@Repository
public interface UserRepository extends JpaRepository<User, String>{
}
