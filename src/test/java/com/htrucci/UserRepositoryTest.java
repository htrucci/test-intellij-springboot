package com.htrucci;

import com.htrucci.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

/**
 * Created by Htrucci on 2017. 11. 6..
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest{
    @Autowired
    private UserRepository userRepository;

    @Test
    public void contextLoads() throws Exception {
        userRepository.save(new User("HKB","123","황"));
        User selectedUser = userRepository.findOne("HKB");
        assertEquals("HKB", selectedUser.getId());
    }
}